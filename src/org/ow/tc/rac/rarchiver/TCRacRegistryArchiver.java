package org.ow.tc.rac.rarchiver;

import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Image;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.DeflaterInputStream;
import java.util.zip.InflaterInputStream;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class TCRacRegistryArchiver {

	private static final String	compressedSuff		= ".gz";
	private static final String	fileNotSelectedMsg	= "File not selected";

	public static void main(String[] args) {
		FileDialog fd = null;
		FileInputStream fis = null;
		FilterInputStream pis = null;
		FileOutputStream fos = null;

		try {
			fd = new FileDialog((Frame) null, "Open file \"RegistryLoader\"", FileDialog.LOAD);
			List<Image> list = IntStream.rangeClosed(0, 4).mapToObj(value -> new ImageIcon(TCRacRegistryArchiver.class
					.getResource("icons/icon_" + ((int) (16 * Math.pow(2, value))) + ".png")).getImage()).collect(Collectors.toList());
			fd.setIconImages(list);
			fd.setMultipleMode(false);
			fd.setLocationRelativeTo(null);
			fd.setVisible(true);
			File[] inputFiles = fd.getFiles();
			if (inputFiles.length == 0 || inputFiles[0] == null) throw new FileNotFoundException(fileNotSelectedMsg);
			String inputFilePath = inputFiles[0].getAbsolutePath();
			boolean doDecompress = inputFilePath.endsWith(compressedSuff);

			fis = new FileInputStream(inputFiles[0]);
			pis = doDecompress ? new InflaterInputStream(fis) : new DeflaterInputStream(fis);

			fd.setTitle("Save " + (doDecompress ? "decompressed" : "compressed") + " file \"RegistryLoader\"");
			fd.setMode(FileDialog.SAVE);
			fd.setFile(doDecompress ? inputFilePath.substring(0, inputFilePath.length() - compressedSuff.length())
					: inputFilePath + compressedSuff);
			fd.setVisible(true);
			File[] outputFiles = fd.getFiles();
			if (outputFiles.length == 0 || outputFiles[0] == null) throw new FileNotFoundException(fileNotSelectedMsg);

			outputFiles[0].createNewFile();
			fos = new FileOutputStream(outputFiles[0]);

			byte[] buffer = new byte[1024];
			for (int length; (length = pis.read(buffer)) != -1;) {
				fos.write(buffer, 0, length);
			}
		} catch (Exception e) {
			if (!fileNotSelectedMsg.equals(e.getMessage())) {
				JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		} finally {
			if (fd != null) fd.dispose();
			closeStream(fis);
			closeStream(pis);
			closeStream(fos);
		}
	}

	private static void closeStream(Closeable stream) {
		if (stream == null) return;
		try {
			stream.close();
		} catch (IOException e) {
		}
	}
}
